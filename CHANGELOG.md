# CHANGELOG

## [1.0.0] - 2021-09-26

### Added

- **Help option :** See all the command line options (--help).
- **Infos option :** See all containers of your docker playground (--infos).
- **Create option :** Create your docker playground (--create \<nb container>).
- **Drop option :** Delete your dokcer playground (--drop).
- **Ansible option :** Init an ansible directory connect to your docker playground (--ansible).
- **Start option :** Start your docker playground (--start).

### Changed

### Fixed

### Remove
