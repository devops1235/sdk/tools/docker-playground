# Docker playground CLI

## Installation

Run the following command.

```bash
./installer.sh # it'll be copy the docker-playground.sh to /usr/local/bin/docker-playground
```



## Command line options

```bash
Options :
		- --create : lancer des conteneurs

		- --drop : supprimer les conteneurs créer par le deploy.sh
	
		- --infos : caractéristiques des conteneurs (ip, nom, user...)

		- --start : redémarrage des conteneurs

		- --ansible : déploiement arborescence ansible
```

## Resources

Based on Xavki repo on Gitlab ([https://gitlab.com/xavki/presentation-ansible-fr/-/blob/master/14-plateforme-dev-docker/](https://gitlab.com/xavki/presentation-ansible-fr/-/blob/master/14-plateforme-dev-docker/)).

Follow him on youtube : [https://www.youtube.com/channel/UCs_AZuYXi6NA9tkdbhjItHQ](https://www.youtube.com/channel/UCs_AZuYXi6NA9tkdbhjItHQ).
